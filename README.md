# Ansible role: labocbz.prepare_host

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![CI Status](https://img.shields.io/badge/CI-success-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Cbz-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)

An Ansible role to prepare your host to work with Ansible.

In your software development, creating a consistent and ready-to-use development environment on Linux servers is vital. However, manually configuring each server to install essential packages, create user accounts, and establish necessary permissions can be a tedious and error-prone task. To address this challenge, this Ansible role automates this process, ensuring that each server is prepared for use.

This Ansible role was designed to automate the initial setup of servers. It automatically installs the required basic packages, creates user accounts, and configures access rights, thereby establishing the necessary foundation for Ansible development or just prepare a server to be used with Ansible. This role saves valuable time and maintains consistency in server configuration, allowing development teams to concentrate on their work while ensuring that servers meet their requirements in a standardized manner.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

Here you can put your change to keep a trace of your work and decisions.

### 2023-04-27: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-04-28: Update / Clean and autoremove packages

* Added an update / clean / autoremove / purge task

### 2023-06-11: Open Sources and Cache Update

* Moove into a new repos for open sources
* Adding codeowners
* Adding group and user creation
* Adding more simple pipeline for testing
* Remonving autoremove / purge / dist upgrade because RedHat ubi8 (YUM) is not compatible

### 2023-06-25: Refactoring vars

* Sudoers and users are in the same liste

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-12-14: System users

* Role can now create system users and address groups

### 2023-12-17: Iptables

* Role create now a cron job to save iptables rules
* Role also create a cron job to restore iptables rules

### 2023-02-18: New CICI and fixes

* Added support for Ubuntu 22
* Added support for Debian 11/22
* Edited vars for linting (role name and __)
* Added generic support for Docker dind (can add used for obscures reasons ... user in use)
* Fix idempotency
* Added informations for UID and GID for user/groups
* Added support for user password creation (on_create)
* New CI, need work on tag and releases
* CI use now Sonarqube
* You can set or not the uid/gid

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch
* Added Timezone setting

### 2024-10-07: Ansible Vault and automations

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation
* Removed uid/gid

### 2024-10-11: Fix users Shell

* Set the default Shell for user
* Fix idempotency on runs

### 2024-11-17: Fix no system user password

* Fix for no system users
* Remove idempotency since edit user's hash password make changes

### 2024-12-30: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube
* Role don't use sudoers template but check of string in file; use lininfile instead

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
