---
- name: "Update / clean and dist-upgrade the host"
  retries: 3
  delay: 10
  until: "output is success"
  register: "output"
  ansible.builtin.package:
    update_cache: true
    autoclean: true
    autoremove: true

- name: "Install packages"
  when: "prepare_host__packages_to_install is defined"
  register: "output"
  until: "output is succeeded"
  retries: 3
  ansible.builtin.package:
    update_cache: true
    name: "{{ prepare_host__packages_to_install }}"

- name: "Ensure sudoers line for users"
  loop: "{{ prepare_host__users_to_add | default([]) }}"
  loop_control:
    loop_var: "user"
  when: "user.is_sudoer | default(false)"
  ansible.builtin.lineinfile:
    path: "/etc/sudoers"
    state: "present"
    regexp: "^{{ user.login }} ALL=(ALL:ALL) ALL"
    line: "{{ user.login }} ALL=(ALL:ALL) ALL"
    validate: "visudo -cf %s"

- name: "Create all needed groups"
  loop: "{{ prepare_host__users_to_add | default([]) }}"
  loop_control:
    loop_var: "user_to_add"
  ansible.builtin.group:
    name: "{{ user_to_add.group }}"
    state: "present"

- name: "Add system users"
  loop: "{{ prepare_host__users_to_add | default([]) }}"
  loop_control:
    loop_var: "user_to_add"
  when: "user_to_add.is_system | default(false)"
  ansible.builtin.user:
    name: "{{ user_to_add.login }}"
    group: "{{ user_to_add.group }}"
    groups: "{{ user_to_add.groups | default(user_to_add.group) }}"
    append: false
    state: "present"
    system: true
    create_home: false
    shell: "/sbin/nologin"

- name: "Add no system users"
  loop: "{{ prepare_host__users_to_add | default([]) }}"
  loop_control:
    loop_var: "user_to_add"
  when: "not (user_to_add.is_system | default(false))"
  ansible.builtin.user:
    name: "{{ user_to_add.login }}"
    group: "{{ user_to_add.group }}"
    groups: "{{ user_to_add.groups | default(user_to_add.group) }}"
    append: false
    state: "present"
    shell: "/bin/bash"
    password: "{{ user_to_add.password | password_hash('sha512') }}"

- name: "Reset current user primary group"
  ansible.builtin.user:
    name: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    state: "present"

- name: "Handle iptables rules saving"
  when: "ansible_distribution == 'Ubuntu' or ansible_distribution == 'Debian'"
  block:
    - name: "Install iptables-persistent"
      retries: 3
      delay: 10
      until: "output is success"
      register: "output"
      ansible.builtin.package:
        name: "iptables-persistent"

    - name: "Add iptables saving action swith cron task"
      ansible.builtin.cron:
        cron_file: "ansible__iptables_persistent"
        name: "Iptables SAVE"
        weekday: "*"
        minute: "*/5"
        hour: "*"
        user: "root"
        job: "$(/usr/sbin/iptables-save > /etc/iptables/rules.v4) > /dev/null 2>&1"
        state: "present"

    - name: "Add iptables restore action with cron task"
      ansible.builtin.cron:
        cron_file: "ansible__iptables_persistent"
        name: "Iptables RESTORE"
        special_time: "reboot"
        user: "root"
        job: "$(/usr/sbin/iptables-restore < /etc/iptables/rules.v4) > /dev/null 2>&1"
        state: "present"

- name: "Set the timezone on the host"
  changed_when: false
  ansible.builtin.shell: |
    echo "{{ prepare_host__timezone }}" > /etc/timezone && \
    timedatectl set-timezone {{ prepare_host__timezone }} && \
    dpkg-reconfigure -f noninteractive tzdata
